package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
public class APIController {

    @GetMapping("/api/call/web/client")
    public Map<String, Object> apiCallAll2() {
        long start = System.currentTimeMillis();
        WebClient webClient = WebClient.create("http://localhost:8080");
        Mono<String> result1 = webClient.get().uri("/delay/api/1").retrieve().bodyToMono(String.class).subscribeOn(Schedulers.elastic());
        Mono<String> result2 = webClient.get().uri("/delay/api/2").retrieve().bodyToMono(String.class).subscribeOn(Schedulers.elastic());
        Mono<String> result3 = webClient.get().uri("/delay/api/3").retrieve().bodyToMono(String.class).subscribeOn(Schedulers.elastic());
        List<Object> results = Mono.zip(result1, result2, result3).block().toList();
        long end = System.currentTimeMillis();
        Map<String, Object> map = new HashMap<>();
        map.put("results", results);
        map.put("duringSeconds", (end - start) / 1000);
        return map;
    }

    @GetMapping("/api/call/rest/template")
    public Map<String, Object> apiCallAll() {
        RestTemplate restTemplate = new RestTemplate();
        List<String> list = new ArrayList<>();
        long start = System.currentTimeMillis();
        list.add(restTemplate.getForObject("http://localhost:8080/delay/api/1", String.class));
        list.add(restTemplate.getForObject("http://localhost:8080/delay/api/2", String.class));
        list.add(restTemplate.getForObject("http://localhost:8080/delay/api/3", String.class));
        long end = System.currentTimeMillis();
        Map<String, Object> map = new HashMap<>();
        map.put("list", list);
        map.put("duringSeconds", (end - start) / 1000);
        return map;
    }

    @GetMapping("/delay/api/1")
    public String delayAPI1() throws InterruptedException {
        System.out.println("call() /delay/api/1");
        TimeUnit.SECONDS.sleep(5L);
        return "test1";
    }

    @GetMapping("/delay/api/2")
    public String delayAPI2() throws InterruptedException {
        System.out.println("call() /delay/api/2");
        TimeUnit.SECONDS.sleep(4L);
        return "test2";
    }

    @GetMapping("/delay/api/3")
    public String delayAPI3() throws InterruptedException {
        System.out.println("call() /delay/api/3");
        TimeUnit.SECONDS.sleep(3L);
        return "test3";
    }
}
